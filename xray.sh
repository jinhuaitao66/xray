#!/bin/bash

# 函数定义 - 安装 Xray
install_xray() {
    bash -c "$(curl -L https://jht126.eu.org/https://github.com/jinhuaitao/Xray-install/blob/main/install-release.sh)" @ install -u root --version 1.8.11
    # 下载配置文件
    curl -o /usr/local/etc/xray/config.json https://gitlab.com/jinhuaitao66/xray/-/raw/main/config.json
    # 重启 Xray 服务
    systemctl restart xray
    # 查看 Xray 服务状态
    systemctl status xray
}

# 函数定义 - 卸载 Xray
uninstall_xray() {
    bash -c "$(curl -L https://jht126.eu.org/https://github.com/jinhuaitao/Xray-install/blob/main/install-release.sh)" @ remove
}

# 主程序
echo "选择要执行的操作:"
echo "1. 安装 Xray"
echo "2. 卸载 Xray"
read -p "请输入数字选择操作: " choice

case $choice in
    1)
        echo "开始安装 Xray..."
        install_xray
        ;;
    2)
        echo "开始卸载 Xray..."
        uninstall_xray
        ;;
    *)
        echo "无效的选择！"
        ;;
esac
